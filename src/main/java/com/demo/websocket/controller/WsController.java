package com.demo.websocket.controller;


import com.demo.websocket.domain.JsonMsg;
import com.demo.websocket.service.WsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("websocket")
public class WsController {
    @Autowired
    public WsService wsService;

    //获取 sessionMap
    @GetMapping("getAddress")
    public JsonMsg getAddress (){
        String URL = "https://whois.pconline.com.cn/ipJson.jsp";
        return  wsService.getAddress(URL);
    }

    //给客服发邮件,提醒有用户上线
    @GetMapping("sendEmail")
    public JsonMsg sendEmail (String email){

        return  wsService.sendEmail(email);
    }


}
