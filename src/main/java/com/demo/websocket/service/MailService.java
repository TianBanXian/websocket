package com.demo.websocket.service;



import com.demo.websocket.domain.JsonMsg;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public interface MailService {

    JsonMsg sendSimpleMail(String to, String subject, String content);
    JsonMsg sendFileMail(String to, String subject, String content, List<File> fileList);
    JsonMsg sendHtmlMail(String to, String subject, String content, Map<String, String> srcIdMap);
    JsonMsg sendHtmlTemplateMail(String to, String subject, Map<String, Object> emailInfoMap);
}
