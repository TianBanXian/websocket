package com.demo.websocket.service;



import com.demo.websocket.domain.JsonMsg;
import org.springframework.stereotype.Service;

@Service
public interface WsService {
    JsonMsg getAddress(String url);

    JsonMsg sendEmail(String email);
}
