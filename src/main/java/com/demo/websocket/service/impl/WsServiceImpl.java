package com.demo.websocket.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import com.demo.websocket.domain.JsonMsg;
import com.demo.websocket.service.MailService;
import com.demo.websocket.service.RedisService;
import com.demo.websocket.service.WsService;
import com.demo.websocket.utils.IPUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class WsServiceImpl implements WsService {
    @Autowired
    public RedisService redisService;
    @Autowired
    public MailService mailService;

    @Override
    public JsonMsg getAddress(String url) {
        String ip ;
        String addr ;
        //nanoid 比uuid效率更高
        String simpleUUID = NanoIdUtils.randomNanoId();
        try {
            String result = IPUtils.doGet(url);
            String address = StringUtils.substring(result,34,result.length()-3);

            Map mapTypes = JSON.parseObject(address);

            ip = ((JSONObject) mapTypes).getString("ip");
            addr = ((JSONObject) mapTypes).getString("pro")+((JSONObject) mapTypes).getString("city")+"的网友";

        } catch (Exception e) {
            String[] strArr = new String[]{"聪明的网友","健康的网友","机智的网友","勇敢的网友","帅气的网友"};
            Random r = new Random();
            ip = "*.*.*.*";
            addr = strArr[r.nextInt(strArr.length)];
            // e.printStackTrace();
        }
        Map<String,Object> userInfo = new HashMap<>();
        userInfo.put("ip",ip);
        userInfo.put("uuid",simpleUUID);
        userInfo.put("nickname",addr);
        //把生成的信息存到redis中,以便MyWebSocket.onOpen 的时候查询UUID对应的Nickname
        redisService.set("USER_INFO:"+simpleUUID,userInfo);
        System.out.println(simpleUUID+",userInfo已保存到redis中");
        return new JsonMsg<>(1,"获取成功",userInfo);
    }

    @Override
    public JsonMsg sendEmail(String email) {
        //验证邮箱格式
        if (ObjectUtils.isEmpty(email)) {
            String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9]+)+"; // (注：\为转义字符）
            if (!email.matches(regex)) {
                return new JsonMsg<>(2, "邮箱格式错误", null);
            }
        }

        String nowTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        /**
         * 发送普通邮件
         *
         * @param to      收件人
         * @param subject 主题（标题）
         * @param content 内容
         */

        return mailService.sendSimpleMail(email,nowTime+"_顾客正在咨询,请尽快上线",nowTime+"_顾客正在咨询,请尽快上线");

    }


}
