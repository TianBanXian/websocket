package com.demo.websocket.service.impl;


import com.demo.websocket.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {
    @Autowired
    private RedisTemplate redisTemplate;
    //保存数据
    @Override
    public boolean set(String key, Object val) {
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //保存数据 带失效时间
    public boolean set(final String key, Object val, long timeAsSecond) {
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            operations.set(key, val);
            redisTemplate.expire(key, timeAsSecond, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    //获取数据
    @Override
    public Object get(String key) {
        try {
            ValueOperations<Serializable, Object> operations = redisTemplate.opsForValue();
            return operations.get(key);
        } catch (Exception e) {
            return null;
        }
    }
    // 存值到redis的Map中
    @Override
    public boolean setMap(String key, String itemKey, Object val) {
        try {
            redisTemplate.opsForHash().put(key, itemKey, val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    // 存值到redis的Map中 带失效时间
    @Override
    public boolean setMap(String key, String itemKey, Object val, long timeAsSecond) {
        try {
            redisTemplate.opsForHash().put(key, itemKey, val);
            redisTemplate.expire(key, timeAsSecond, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    // 取值 Map 所有
    @Override
    public Object getMap(String key) {
        try {
            return redisTemplate.opsForHash().entries(key);
        } catch (Exception e) {
            return null;
        }
    }
    // 取值 Map
    @Override
    public Object getMap(String key, String itemKey) {
        try {
            return redisTemplate.opsForHash().get(key, itemKey);
        } catch (Exception e) {
            return null;
        }
    }
    //删除key
    @Override
    public boolean deleteByKey(String key) {
        return redisTemplate.delete(key);
    }
    //删除Map下的指定key
    @Override
    public boolean deleteMap(String key, String itemKey) {
        long deleted = redisTemplate.opsForHash().delete(key, itemKey);
        return deleted != 0 ? true : false;
    }
    //添加到List
    @Override
    public boolean setList(String key, Object val) {
        long added = redisTemplate.opsForList().rightPush(key, val);
        return added != 0 ? true : false;
    }
    //添加到List 带失效时间
    @Override
    public boolean setList(String key, Object val, long timeAsSecond) {
        long added = redisTemplate.opsForList().rightPush(key, val);
        redisTemplate.expire(key, timeAsSecond, TimeUnit.SECONDS);
        return added != 0 ? true : false;
    }
    //获取List中所有数据
    @Override
    public Object getList(String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    @Override
    public Object getList(String key, long idx) {
        return redisTemplate.opsForList().range(key, 0, -1).get((int) idx);
    }

    @Override
    public Set<String> getKeys(String key) {
        return redisTemplate.keys(key);

    }

}
