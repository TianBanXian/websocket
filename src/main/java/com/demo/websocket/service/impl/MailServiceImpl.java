package com.demo.websocket.service.impl;




import com.demo.websocket.domain.JsonMsg;
import com.demo.websocket.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {

    @Value("${spring.mail.username}")
    private String SENDER ;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 发送普通邮件
     *
     * @param to      收件人
     * @param subject 主题（标题）
     * @param content 内容
     */
    public JsonMsg sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(SENDER);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        try {
            mailSender.send(message);
            System.out.println("向"+to+"邮箱发送内容为:["+content+"]的邮件成功!");
            return new JsonMsg<>(200,"发送普通邮件成功",null);
        } catch (Exception e) {
            return new JsonMsg<>(500,"发送普通邮件时发生异常",e);
        }
    }

    /**
     * 发送带附件的邮件
     *
     * @param to       收件人
     * @param subject  主题
     * @param content  内容
     * @param fileList 附件
     */
    public JsonMsg sendFileMail(String to, String subject, String content, List<File> fileList) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            //true表示需要创建一个multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(SENDER);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);

            if (fileList != null && fileList.size() > 0) {
                for (File file : fileList) {
                    FileSystemResource fileSystemResource = new FileSystemResource(file);
                    String fileName = fileSystemResource.getFilename();
                    helper.addAttachment(fileName, fileSystemResource);
                }
            }
            mailSender.send(message);
            return new JsonMsg<>(200,"发送带附件的邮件成功",null);
        } catch (MessagingException e) {
            return new JsonMsg<>(500,"发送带附件的邮件时发生异常",e);
        }
    }

    /**
     * 发送HTML静态文件的邮件
     *
     * @param to       收件人
     * @param subject  主题
     * @param content  内容
     * @param srcIdMap 需要替换的静态文件(给html中标记id不赋值的话，可以发送，但静态文件不会显示)，可为null表示纯HTML文件
     */
    public JsonMsg sendHtmlMail(String to, String subject, String content, Map<String, String> srcIdMap) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            //true表示需要创建一个multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(SENDER);
            helper.setTo(to);
            helper.setSubject(subject);
            //true指的是html邮件
            helper.setText(content, true);
            if (srcIdMap != null) {
                for (Map.Entry<String, String> entry : srcIdMap.entrySet()) {
                    FileSystemResource file = new FileSystemResource(new File(entry.getValue()));
                    helper.addInline(entry.getKey(), file);
                }
            }
            mailSender.send(message);
            return new JsonMsg<>(200,"发送HTML静态文件的邮件成功",null);
        } catch (MessagingException e) {
            return new JsonMsg<>(500,"发送HTML静态文件的邮件时发生异常",e);
        }
    }

    /**
     * 发送Html模板的邮件
     *
     * @param to       收件人
     * @param subject  主题
     * @param emailInfoMap 模板的数据信息
     */
    public JsonMsg sendHtmlTemplateMail(String to, String subject, Map<String, Object> emailInfoMap) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            //true表示需要创建一个multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(SENDER);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setSentDate(new Date());
            // 设置参数, 也可以单独设置context.setVariable("userName", "赵云");
            Context context = new Context();
            context.setVariables(emailInfoMap);
            //设置模板，emailTemplate为html文件的文件名，emailTemplate.html或emailTemplate都可以
            helper.setText(templateEngine.process("emailTemplate", context), true);
            mailSender.send(message);
            return new JsonMsg<>(200,"发送Html模板的邮件成功",null);
        } catch (MessagingException e) {
            return new JsonMsg<>(500,"发送Html模板的邮件",e);
        }
    }

}

