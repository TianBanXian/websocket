package com.demo.websocket.service;

import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface RedisService {
    //保存数据
    boolean set(final String key, Object val);
    //保存数据 带失效时间
    boolean set(final String key, Object val, long timeAsSecond);
    //获取数据
    Object get(final String key);
    // 存值到redis的Map中
    boolean setMap(final String key, String itemKey, Object val);
    // 存值到redis的Map中 带失效时间
    boolean setMap(final String key, String itemKey, Object val, long timeAsSecond);
    // 取值 Map 所有
    Object getMap(final String key);
    // 取值 Map
    Object getMap(final String key, String itemKey);
    //删除key
    boolean deleteByKey(final String key);
    //删除Map下的指定key
    boolean deleteMap(final String key, String itemKey);
    //添加到List
    boolean setList(final String key, Object val);
    //添加到List 带失效时间
    boolean setList(final String key, Object val, long timeAsSecond);
    //获取List中所有数据
    Object getList(final String key);
    //获取list中指定索引的数据
    Object getList(final String key, long idx);

    Set<String> getKeys (final String key);
}
