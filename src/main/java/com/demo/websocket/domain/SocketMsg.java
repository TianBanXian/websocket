package com.demo.websocket.domain;

import cn.hutool.core.date.DateTime;

import java.util.Date;

/**
 * 定义Message 的JSON消息格式,方便java根据pojo解析,目前前后端均未格式化时间
 *
 * java时间格式化：
 * new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
 *
 * js时间格式化：自己写方法网上有
 * (new Date()).format("yyyy-mm-dd hh:mm:ss")
 *
 */


public class SocketMsg {

    private int type;   //聊天类型0：群聊，1：单聊. 目前项目未使用群里功能 这个字段暂保留,留作未来升级
    private int msgType; //消息类型 0 首次连线通知  1 普消消息  2 下线通知  3上线通知
    private int sendType; //发送类型 0是用户,1是客服 为了方便js渲染聊天记录 及 sql中更少的保存字段

    private String ip; //用户的ip
    private String msg;//消息
    private String msgTime; //消息时间

    private String fromUser;//发送者.
    private String fromName; //发送者姓名

    private String toUser;//接受者.
    private String toName; //接受者姓名




    @Override
    public String toString() {
        return "SocketMsg{" +
                "type=" + type +
                ", fromUser='" + fromUser + '\'' +
                ", toUser='" + toUser + '\'' +
                ", msg='" + msg + '\'' +
                ", fromName='" + fromName + '\'' +
                ", toName='" + toName + '\'' +
                ", msgTime=" + msgTime +
                ", sendType=" + sendType +
                ", msgType=" + msgType +
                ", ip='" + ip + '\'' +
                '}';
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public int getSendType() {
        return sendType;
    }

    public void setSendType(int sendType) {
        this.sendType = sendType;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public SocketMsg(int type, String fromUser, String toUser, String msg, String fromName, String toName, String msgTime, int sendType, int msgType, String ip) {
        this.type = type;
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.msg = msg;
        this.fromName = fromName;
        this.toName = toName;
        this.msgTime = msgTime;
        this.sendType = sendType;
        this.msgType = msgType;
        this.ip = ip;
    }

    public SocketMsg() {
    }
}